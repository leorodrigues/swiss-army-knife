package org.leonardo.sak.tools.shell.routing;

import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.util.Optional;

public class RoutesDispatcher {

    private RouteCollection routes;

    public void dispatchPost(String url, String payloadPath) {
        Optional<Post> method = this.routes.find(url);
    }

    public void dispatchPut(String url, String payloadPath) {

    }

    public void dispatchDelete(String url) {

    }

    public void dispatchGet(String url) {
    }

    public void onPost(Route route, RouteAction action) {
        this.routes.add(new Post(route, action));
    }
}
