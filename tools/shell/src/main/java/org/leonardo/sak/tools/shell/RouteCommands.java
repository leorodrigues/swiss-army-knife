package org.leonardo.sak.tools.shell;

import org.leonardo.sak.tools.shell.routing.RoutesDispatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
@ShellCommandGroup("Routing commands")
public class RouteCommands {

    private static final String END_OF_COMMAND = "End of command";

    private RoutesDispatcher routesDispatcher;

    @Autowired
    public RouteCommands(RoutesDispatcher routesDispatcher) {
        this.routesDispatcher = routesDispatcher;
    }

    @ShellMethod(key = { "post" }, value = "Send a POST request")
    public String handlePost(@ShellOption(help = "Resource URL") String url,
        @ShellOption(value = { "-p" }, defaultValue = "", help = "Payload path")
            String payloadPath) {
        routesDispatcher.dispatchPost(url, payloadPath);
        return END_OF_COMMAND;
    }

    @ShellMethod(key = { "put" }, value = "Send a PUT request")
    public String handlePut(@ShellOption(help = "Resource URL") String url,
        @ShellOption(value = { "-p" }, defaultValue = "", help = "Payload path")
            String payloadPath) {
        routesDispatcher.dispatchPut(url, payloadPath);
        return END_OF_COMMAND;
    }

    @ShellMethod(key = { "del" }, value = "Send a DELETE request")
    public String handleDelete(@ShellOption(help = "Resource URL") String url) {
        routesDispatcher.dispatchDelete(url);
        return END_OF_COMMAND;
    }

    @ShellMethod(key = { "get" }, value = "Send a GET request")
    public String handleGet(@ShellOption(help = "Resource URL") String url) {
        routesDispatcher.dispatchGet(url);
        return END_OF_COMMAND;
    }
}
