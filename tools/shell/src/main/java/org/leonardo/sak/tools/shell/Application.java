package org.leonardo.sak.tools.shell;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.leonardo.sak.tools.shell.routing.RoutesDispatcher;
import org.leonardo.sak.tools.shell.routing.RoutesHandler;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.shell.jline.PromptProvider;

import static org.jline.utils.AttributedStyle.DEFAULT;
import static org.jline.utils.AttributedStyle.YELLOW;
import static org.springframework.beans.factory.annotation.Autowire.BY_TYPE;
import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
public class Application {

    private static AttributedStyle PROMPT_STYLE = DEFAULT.foreground(YELLOW);

    public static void main(String[] args) {
        run(Application.class, args);
    }

    @Bean
    public PromptProvider makePromptProvider() {
        return () -> new AttributedString("sak:> ", PROMPT_STYLE);
    }

    @Bean(autowire = BY_TYPE)
    public RoutesDispatcher makeRouteDispatcher(RoutesHandler handler) {
        RoutesDispatcher dispatcher = new RoutesDispatcher();

//        dispatcher.onPost("/environment",
//            handler.postEnvironment());
//
//        dispatcher.onGet("/environment",
//            handler.getAllEnvironments());
//
//        dispatcher.onGet("/environment/:id",
//            handler.getEnvironmentById());
//
//        dispatcher.onGet("/routes",
//            handler.getAllRoutes());

        return dispatcher;
    }

    @Bean(autowire = BY_TYPE)
    public RoutesHandler makeRoutesHandler() {
        return new RoutesHandler();
    }
}
